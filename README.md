# Getting Started with Create React App

State Management tool - Redux/Mobx/Recoil vs react query

Before couple of years, it was only about component driven approach and state management. We never thought about ui state and server. But Over last couple of years, React community has come to realize that "data fetching and caching" is really a different set of concerns than "State management".

In more elaborated way, state of our application can be described as mix of two types of data:

1.  UI state: The kind of state that is derived from the user interaction with certain piece of the UI like modals, alerts, even authentication. We have a complete control and ownership of this state. The type of state that gets killed everytime we refresh the browser. It is synchronous and easy to retrieve.
2.  Server state or cache state: The kind of data that is remotely persisted, asynchronous with shared ownership that can be potentially out of date.

Following are the Questions that I came up to decide about the business logic and server data

## Why do we think that we need a state management tool - Redux specifically?

- React is in its core a state management solution, it offers the required tools to manage the state that belongs to the UI by itself but when we need to deal with what we call server state then React is not enough and we need to go out and look for 3rd party.
- It helps to manage our application state - whether domain state or View state together.
- It keeps the state in global to avoid props drilling and We can easily communicate the change with different component whether they stand in hierarchy or not.
- It gives us a opinionated way to achieve our task.
- It provides a way to add middleware which in turn helps in extending Redux.(Follows open close principle). For example - redux-thunk(async operation), redux-lifesave(throttle actions), reedux-actions-log(action logs), redux-batched-updates etc.
- It does provide a way to a cache server response using RTK query(https://redux-toolkit.js.org/rtk-query/overview)

## What developers end up doing and as result What we don't like about the Redux?

- Since it enforces us to use opinionated approach we end up creating boiler plate code for trivial scenarios. For instance may be modal popup open close state, then we need to create the actions, reducers for them too.
- When we rely on avoiding props driling and start putting every state in global State - Redux, We end up rerender the components which are not even associated with those changes, as result performance impact in our React application.
- We also end up forget about the concept Colocating state hence again performance impact.

Why prefer functional component with hooks over class component?

## Why do we need hooks/custom hooks?

1. Uptill before React 16.8v, we didnot had a hooks in place, we never had a way to abstract the business logic to some services or may be model, provided if we are not using Redux or something. Now with the introduction of hooks, we can create custom hooks to encapsulate the business logic away from components.
2. After the realization of problems of component with business logic, react community emerged with pattern use custom hooks to abstract the business logic.
3. Abstraction does provide a layer where under the hood we can change the library to other (for intance - react query, graphql or may be something custom ) in case of change in requirement or may be anything.
4. Earlier we did manual change detection in componentDidUpdate whereas now we have useEffect with dependecy of properties and it will automatically synchronize for us.
5. Custom hooks provides us:
6. to build our own logic
7. Much more portable and sharable logic
8. Component-aware abstractions
9. Rapid iteration.

### Example of custom hooks

- usePermissions, useLoading, useClipboard, useDebounce, useSubscription, useForm, useToast, useAuth, useTooltip, usePagination, useModal, useUser etc

### Use cases of custom hooks:

1. Portable ui utitlities
2. Global state and
3. Server state
4. Encapsulates business logic.

## Why we can choose React query to manage server data?

- React query handles the whole state of the fetching process as a state machine meaning that is not possible to fall into an impossible state.
- It manages the cache giving the app instant refresh and background process to update the data in place
- Cancel ongoing queries that can affect the optimistic update
- rollback the mutation in case of error.
- request zero config and almost zero maintenance

## Pattern that was imerged in past year (may be earlier in 2020)

Components === User interface (handles)
hooks === Business logic (handles)
UI -> Business logic and UI logic -> Data services intergrations utilities (custom hook, store(app or 3rd party) or API)

Reference Links:
https://blog.testdouble.com/posts/2021-05-03-reduce-state-management-with-react-query/
https://kentcdodds.com/blog/application-state-management-with-react
https://kentcdodds.com/blog/state-colocation-will-make-your-react-app-faster
https://kentcdodds.com/blog/optimize-react-re-renders
https://clevertech.biz/insights/why-we-choose-react-query-as-a-server-state-management-solution

- eslint-plugin-react-hooks - fills up the dependency on useEffect for us with every useEffect

- unnecessary renders? Everytime global state changes or dispatch, every components renders again.
- multiple contexts used to avoid unnecessary renders

Pattern that imerged about the introduction of hooks and identify UI state and server state, use of custom hooks to abstract the business logic and keep the components clean.

- usePromise that will return loading, error etc.
